package elevator

import (
	"time"
)

type Elevator struct {
	Events []Event
}

type Event interface {
	GetTime() time.Time
}

type TransitingElevatorMovementEvent struct {
	FromFloor *Floor
	ToFloor   *Floor
}

func (e *Elevator) Move() {
	event := TransitingElevatorMovementEvent{
		FromFloor: nil,
		ToFloor:   nil,
	}

	e.Events = append(e.Events, event)
}
func (e TransitingElevatorMovementEvent) GetTime() time.Time {
	return time.Now()
}
