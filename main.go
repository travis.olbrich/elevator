package main

import (
	"elevator/render"
	"fmt"
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"image/color"
	"time"
)

var image *ebiten.Image
var opts ebiten.DrawImageOptions
var ySpeed float64
var xSpeed float64

var elevator render.Elevator

func update(screen *ebiten.Image) error {
	ebitenutil.DebugPrint(screen, fmt.Sprintf("Up and down to move elevator, space to restrict speed.\n%.3ftps, %.2ffps\nelevator: %v", ebiten.CurrentTPS(), ebiten.CurrentFPS(), elevator))

	if image == nil {
		elevator = render.Elevator{0, 2, 2, 0, time.Now()}
		image, _ = ebiten.NewImage(20, 50, ebiten.FilterDefault)
		opts = ebiten.DrawImageOptions{}
		ySpeed, xSpeed = 0, 0
	}

	image.Fill(color.White)

	if ebiten.IsKeyPressed(ebiten.KeySpace) {
		elevator.SpeedLimit = .1
	} else if !ebiten.IsKeyPressed(ebiten.KeySpace) {
		elevator.SpeedLimit = 2
	}

	if ebiten.IsKeyPressed(ebiten.KeyDown) {
		elevator.MoveDown()
	} else if ebiten.IsKeyPressed(ebiten.KeyUp) {
		elevator.MoveUp()
	} else {
		elevator.Stop()
	}

	elevator.UpdatePosition()

	if ebiten.IsDrawingSkipped() {
		return nil
	}

	opts.GeoM.Reset()
	opts.GeoM.Translate(200, 50)
	opts.GeoM.Translate(0, -elevator.Height)
	screen.DrawImage(image, &opts)

	return nil
}

func main() {
	ebiten.SetMaxTPS(120)

	ebiten.Run(update, 640, 480, 2, "Hello world!")
}
