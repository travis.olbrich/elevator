package render

import (
	"fmt"
	"github.com/hajimehoshi/ebiten"
	"math"
	"time"
)

type Elevator struct {
	Speed      float64
	SpeedLimit float64
	Accel      float64
	Height     float64

	StartTime time.Time
}

func (e Elevator) String() string {
	return fmt.Sprintf("Speed: %.2f, Limit: %.2f, Accel: %.2f, Height: %.2f", e.Speed, e.SpeedLimit, e.Accel, e.Height)
}

func (e *Elevator) MoveUp() {
	// Up is positive
	e.approachTargetSpeed(e.SpeedLimit)
}

func (e *Elevator) MoveDown() {
	// Down is negative
	e.approachTargetSpeed(-e.SpeedLimit)
}

func (e *Elevator) approachTargetSpeed(targetSpeed float64) {
	targetSpeed = e.restrictToSpeedLimit(targetSpeed)
	diff := targetSpeed - e.Speed
	currentTps := ebiten.CurrentTPS()
	multiplier := 2.0
	dy := e.Accel / (currentTps / multiplier)

	if math.Abs(diff) < dy {
		e.Speed = targetSpeed
		if time.Since(e.StartTime).Nanoseconds() > 100000000 {
			fmt.Println(time.Since(e.StartTime))
		}
		e.StartTime = time.Now()
	} else if diff > 0 {
		e.Speed += dy
	} else if diff < 0 {
		e.Speed -= dy
	}
}

func (e *Elevator) restrictToSpeedLimit(targetSpeed float64) float64 {
	if targetSpeed > e.SpeedLimit {
		targetSpeed = e.SpeedLimit
	} else if targetSpeed < -e.SpeedLimit {
		targetSpeed = -e.SpeedLimit
	}
	return targetSpeed
}

func (e *Elevator) Stop() {
	e.approachTargetSpeed(0)
	e.StartTime = time.Now()
}

func (e *Elevator) UpdatePosition() {
	e.Height += e.Speed
}
